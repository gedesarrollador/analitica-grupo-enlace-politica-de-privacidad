Plítica de Privacidad
---------------------

### Introducción

La presente Política de Privacidad establece los términos en que usa y protege la información que es proporcionada por sus usuarios al momento de utilizar Analítica, Grupo Enlace. Analítica, Grupo Enlace está comprometida con la seguridad de los datos de sus usuarios. Cuando le pedimos llenar los campos de información personal con la cual usted pueda ser identificado, lo hacemos asegurando que sólo se empleará de acuerdo con los términos de este documento. Sin embargo esta Política de Privacidad puede cambiar con el tiempo o ser actualizada por lo que le recomendamos y enfatizamos revisar continuamente esta página para asegurarse que está de acuerdo con dichos cambios.

### Información que es recogida

Nuestro sitio web podrá recoger información personal por ejemplo: Nombre, información de contacto como su dirección de correo electrónica. Así mismo cuando sea necesario podrá ser requerida información específica para realizar algún proceso dentro de la aplicación.

### Uso de la información recogida

Analitica, Grupo Enlace emplea la información con el fin de proporcionar el mejor servicio posible, particularmente para mantener un registro de usuarios y mejorar nuestros productos y servicios. Es posible que sean enviados correos electrónicos a través de nuestro sitio al momento que el usuario desee recuperar su contraseña o al momento de crear el usuario estos correos electrónicos serán enviados a la dirección que usted proporcione.

Analítica, Grupo Enlace está altamente comprometido para cumplir con el compromiso de mantener su información segura. Usamos los sistemas más avanzados y los actualizamos constantemente para asegurarnos que no exista ningún acceso no autorizado.

### Control de su información personal

Analítica, Grupo Enlace no venderá, cederá ni distribuirá la información personal que es recopilada sin su consentimiento, salvo que sea requerido por un juez con un orden judicial.

Se reserva el derecho de cambiar los términos de la presente Política de Privacidad en cualquier momento.

### Informacion de dispositivo  
Recopilamos información de su dispositivo en algunos casos. La información se utilizará para brindar un mejor servicio y prevenir actos fraudulentos. Además, dicha información no incluirá la que identificará al usuario individual.

### Seguridad  
Valoramos la confianza al proveer su informacion personal, por esa razon nos esforzamos por utilizar medios comercialmente aceptables para protegerlo. Pero recuerde que ningún método de transmisión a través de Internet o método de almacenamiento electrónico es 100% seguro y confiable, y no podemos garantizar su absoluta seguridad.

### Servicio de terceros  
En algun momento se hara uso de empresas e individuos de terceros por las siguientes razones: 
* Facilitar nuestro servicio
* Para proporcionar el Servicio en nuestro nombre;
* Para realizar servicios relacionados con el servicio; o
* Para ayudarnos a analizar cómo se utiliza nuestro Servicio.



### Contactenos
Si tiene preguntas o sugerencias con respecto a nuestra Politica de Privacidad, no dude en contactarnos.  
Contact Information:  
Email: *[geanalitica.desarrollador@gmail.com]*  
